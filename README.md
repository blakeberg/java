# Ubuntu mit Oracle JDK #

Dieses Dockerfile ist ein [trusted build](https://registry.hub.docker.com/u/blakeberg/java/) im [Docker Registry](https://registry.hub.docker.com).
Oracle JDK 6u45 ist bereits installiert.

**Oracle Java Lizenz:** 
[http://www.oracle.com/technetwork/java/javase/downloads/java-se-archive-license-1382604.html](http://www.oracle.com/technetwork/java/javase/downloads/java-se-archive-license-1382604.html)

Erforderlich für den Container sind mind. 1 GB freier Plattenplatz und 1 GB Arbeitsspeicher.

## Versionen ##
Die Version ***latest*** entspricht dem ***Git-master*** und beinhaltet die laufende Entwicklung. Stabile und laufende Versionen werden jeweils getaggt:

* **1.0:** erste Version mit offiziellen Baseimage von Ubuntu in getaggter Version 14.4.1

## Installation ##
Es sollte immer eine  Version angeben werden, da sonst die aktuelle in der Entwicklung **latest** stehende Version abgerufen wird.

1. `docker pull blakeberg/java:1.0`
2. `docker run -d -h java --name=java -p 49152:22 blakeberg/java:1.0`

### SSH ###
* `ssh root@localhost -p 49152`
* Passwort: admin